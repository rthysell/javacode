public class Game {

	Board b;
	Player p;

	Game() {
		b = new Board(10,10);
		p = new Player(b.getXSize(), b.getYSize(), b.getGridSize());
	}
	
	public int getMoves() { return p.getNumMoves(); }

	public void MovePlayer(String direction) {
        /* This function allows the player to move their position on the board. If a player attempts to move out of bounds, their character
         * will automatically be bumped back to the previous row or column. So no matter how hard they try, their character will remain
         * within the bounds of the board.
         * Players use the 'w,a,s,d' keys to move their character up, left, down, and right. If the player lands on a tar pit, they lose three
         * moves in addition to the one they just took. A player may press and enter 'q' to quit the game. If the player lands on Manbearpig,
         * a congratulatory message prints to the screen and the game ends. If the player runs out of moves before finding Manbearpig, a
         * "Game Over" message prints to the screen and the game ends.
         */
		if (direction == "w") {
			int temp = p.getPlayerNewX() - 1;
            //Check if moving upward is valid. If so, move, if not, bounce down.
			if (temp > -1) {
				p.moveUp();
				p.removeOneMove();
				if (b.GameBoard[p.getPlayerNewX()][p.getPlayerNewY()] == 9) {
					p.removeOneMove();
					p.removeOneMove();
					p.removeOneMove();
				}
			}
			else {					
				p.moveDown();
			}
		}
		else if (direction == "a") {
			int temp = p.getPlayerNewY() - 1;
            //Check if moving left is valid. If so, move, if not, bounce right.
			if (temp > -1) {
				p.moveLeft();
				p.removeOneMove();
				if (b.GameBoard[p.getPlayerNewX()][p.getPlayerNewY()] == 9) {
					p.removeOneMove();
					p.removeOneMove();
					p.removeOneMove();
				}
			}
			else {
				p.moveRight();
			}
		}
		else if (direction == "s") {
			int temp = p.getPlayerNewX() + 1;
            //Check if moving down is valid. If so, move, if not, bounce up.
			if (temp < b.getXSize()) {
				p.moveDown();
				p.removeOneMove();
				if (b.GameBoard[p.getPlayerNewX()][p.getPlayerNewY()] == 9) {
					p.removeOneMove();
					p.removeOneMove();
					p.removeOneMove();
				}
			}
			else {
				p.moveUp();
			}
		}
		else if (direction == "d") {
			int temp = p.getPlayerNewY() + 1;
            //Check if moving right is valid. If so, move, if not, bounce left.
			if (temp < b.getYSize()) {
				p.moveRight();
				p.removeOneMove();
				if (b.GameBoard[p.getPlayerNewX()][p.getPlayerNewY()] == 9) {
					p.removeOneMove();
					p.removeOneMove();
					p.removeOneMove();
				}
			}
			else {
				p.moveLeft();
			}
		}
		b.RevealEmptySpaces(p.getPlayerNewX(), p.getPlayerNewY()); //Reveal all empty spaces that are 0s, plus the ones surrounding the player.
	}
}