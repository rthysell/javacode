import java.awt.*;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

import javax.swing.*;

public class Display {

	JFrame frame;
	JPanel panel;
	JPanel textPanel;
	JLabel[][] label;
	JLabel numMoves;
	Game g;
	
	private int width, height;

	Display(int x, int y) {
		frame = new JFrame("Welcome to The Race to Manbearpig!");
		frame.setSize(500,500);
		frame.setResizable(true);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setFocusable(true);
		frame.setVisible(true);
		
		g = new Game();
		
		panel = new JPanel();
		panel.setBackground(Color.BLACK);
		
		textPanel = new JPanel();

		width = x;
		height = y;
	}

	void DisplaySetUp(int x, int y) {
		frame = new JFrame("Welcome to The Race to Manbearpig!");
		frame.setSize(500,500);
		frame.setResizable(true);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setFocusable(true);
		frame.setVisible(true);
		
		g = new Game();
		
		panel = new JPanel();
		panel.setBackground(Color.BLACK);
		
		textPanel = new JPanel();

		width = x;
		height = y;
		
		GridSetUp();
	}
	
	private void GridSetUp() {
		panel.setLayout(new GridLayout(width, height,1,1));
		label = new JLabel[width][height];
		
		String moves = String.valueOf(g.p.getNumMoves());
		
		numMoves = new JLabel("Number of moves left: " + moves);
		textPanel.add(numMoves);
		
		ImageIcon tree = new ImageIcon(getClass().getResource("tree.png"));
		ImageIcon player = new ImageIcon(getClass().getResource("algore.png"));
		JLabel temp;
		
		for (int r = 0; r < height; r++) {
			for (int c = 0; c < width; c++) {
				temp = new JLabel("tree", tree, JLabel.CENTER);
				temp.setVerticalTextPosition(JLabel.CENTER);
				temp.setHorizontalTextPosition(JLabel.CENTER);
				temp.setText("");
				label[r][c] = temp;
				panel.add(label[r][c]);
				if (g.b.GameBoard[r][c] == -1) {
					temp = new JLabel("player", player, JLabel.CENTER);
					temp.setVerticalTextPosition(JLabel.CENTER);
					temp.setHorizontalTextPosition(JLabel.CENTER);
					temp.setText("");
					panel.remove(label[r][c]);
					label[r][c] = temp;
					panel.add(label[r][c]);
				}
			}
		}
		frame.getContentPane().add(BorderLayout.NORTH, textPanel);
		frame.getContentPane().add(BorderLayout.CENTER, panel);
		frame.pack();
		addKeyListener();
	}

	
	private void RefreshGrid() {
		ImageIcon tree = new ImageIcon(getClass().getResource("tree.png"));
		ImageIcon player = new ImageIcon(getClass().getResource("algore.png"));
		ImageIcon dirt = new ImageIcon(getClass().getResource("dirt.png"));
		ImageIcon hole = new ImageIcon(getClass().getResource("hole.png"));
		ImageIcon manbearpig = new ImageIcon(getClass().getResource("manbearpig.png"));
		JLabel temp = new JLabel();
		
		textPanel.remove(numMoves);
		String moves;
		moves = String.valueOf(g.p.getNumMoves());
		numMoves = new JLabel("Number of moves left: " + moves);
		textPanel.add(numMoves);
		checkWinner();
		int count = 0;
		
		for (int r = 0; r < height; r++) {
			for (int c = 0; c < width; c++) {
				if (g.b.Visited[r][c] == true) {
					if (r == g.p.getPlayerNewX() && c == g.p.getPlayerNewY()) {
						temp = new JLabel("player", player, JLabel.CENTER);
						temp.setText("");
					}
					else if (g.b.GameBoard[r][c] == 0 || g.b.GameBoard[r][c] == -1) {
						temp = new JLabel("dirt", dirt, JLabel.CENTER);
						temp.setText("");
					}
					else if (g.b.GameBoard[r][c] == 9) {
						temp = new JLabel("hole", hole, JLabel.CENTER);
						temp.setText("");
					}
					else if (g.b.GameBoard[r][c] == 13) {
						temp = new JLabel("manbearpig", manbearpig, JLabel.CENTER);
						temp.setText("");
					}
					else if (g.b.GameBoard[r][c] == 1) {
						temp = new JLabel("1", JLabel.CENTER);
						temp.setForeground(Color.WHITE);
					}
					else if (g.b.GameBoard[r][c] == 2) {
						temp = new JLabel("2", JLabel.CENTER);
						temp.setForeground(Color.WHITE);
					}
					else if (g.b.GameBoard[r][c] == 3) {
						temp = new JLabel("3", JLabel.CENTER);
						temp.setForeground(Color.WHITE);
					}
					else if (g.b.GameBoard[r][c] == 4) {
						temp = new JLabel("4", JLabel.CENTER);
						temp.setForeground(Color.WHITE);
					}
					else if (g.b.GameBoard[r][c] == 5) {
						temp = new JLabel("5", JLabel.CENTER);
						temp.setForeground(Color.WHITE);
					}
					else {
						temp = new JLabel("tree", tree, JLabel.CENTER);
						temp.setText("");
					}
					temp.setVerticalTextPosition(JLabel.CENTER);
					temp.setHorizontalTextPosition(JLabel.CENTER);
					panel.remove(label[r][c]);
					label[r][c] = temp;
					panel.add(label[r][c], count).setVisible(true);
				}
				count++;
			}
		}
		frame.getContentPane().add(BorderLayout.NORTH, textPanel);
		frame.getContentPane().add(BorderLayout.CENTER, panel);
		frame.pack();
	}

	private void checkWinner() {
		if (g.b.GameBoard[g.p.getPlayerNewX()][g.p.getPlayerNewY()] == 13) {
			numMoves = new JLabel("You found Manbearpig! You win!");
			textPanel.add(numMoves);
			resetBoard();
		}
		if (g.p.getNumMoves() <= 0) {
			numMoves = new JLabel("No more moves left! You lose!");
			textPanel.add(numMoves);
			resetBoard();
		}
	}
	
	private void resetBoard() {
		frame.setVisible(false);
		frame.dispose();
		DisplaySetUp(10,10);
		//GridSetUp();
	}
	
	private void addKeyListener() {
		frame.addKeyListener(new KeyListener() {
			String direction = "";
			@Override
			public void keyPressed(KeyEvent e) {
			}
			@Override
			public void keyReleased(KeyEvent e) {
				switch(e.getKeyCode()) {
				case KeyEvent.VK_UP:
					direction = "w";
					break;
				case KeyEvent.VK_LEFT:
					direction = "a";
					break;
				case KeyEvent.VK_DOWN:
					direction = "s";
					break;
				case KeyEvent.VK_RIGHT:
					direction = "d";
					break;
				}
				g.MovePlayer(direction);
				RefreshGrid();
			}
			
			@Override
			public void keyTyped(KeyEvent arg0) {
				// TODO Auto-generated method stub
				
			}
		});
		frame.requestFocus();
	}
	
	public static void main(String[] args) {
		Display display = new Display(10,10);
		display.GridSetUp();
	}

}