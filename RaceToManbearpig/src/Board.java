import java.util.*;

public class Board {
	
	class Point {
		int row;
		int col;

		Point(int r, int c) {
			row = r;
			col = c;
		}
	}

	int [][] GameBoard = new int[10][10];
	Boolean [][] Visited = new Boolean [10][10];
	
	private int xSize, ySize, GridSize;

	Board(int x, int y) {
		xSize = x;
		ySize = y;
		GridSize = xSize * ySize;
		for (int row = 0; row < xSize; row++) {
			for (int col = 0; col < ySize; col++) {
				GameBoard[row][col] = 0;
				Visited[row][col] = false;
			}
		}

		for (int i = 0; i < (GridSize/4); i++) {
			AddMines();
		}
		AddNums();
		AddPlayer();
		AddManBearPig();
	}

	public int getXSize() { return xSize; }
	public int getYSize() { return ySize; }
	public int getGridSize() { return GridSize; }

	private void AddMines() {
		Random generator = new Random();
		int row = generator.nextInt() % xSize;
		int col = generator.nextInt() % ySize;

		if (row < 0) row *= -1;
		if (col < 0) col *= -1;

		if (GameBoard[row][col] != 9) {
			GameBoard[row][col] = 9;
		}
	}

	private void AddNums() {
        /* Add the numbers that surround a tar pit and place them next to the
         * corresponding pit. This will show the grid like a finished Minesweeper game.
         */
		for (int row=0; row<xSize; row++) {
			for (int col=0; col<ySize; col++) {
				if (GameBoard[row][col] == 9) {
					if (row < xSize-1 && row > 0 && col < ySize-1 && col > 0) {
						if (GameBoard[row-1][col] != 9)
							GameBoard[row-1][col] += 1;
						
						if (GameBoard[row+1][col] != 9)
							GameBoard[row+1][col] += 1;
						
						if (GameBoard[row][col-1] != 9)
							GameBoard[row][col-1] += 1;
						
						if (GameBoard[row][col+1] != 9)
							GameBoard[row][col+1] += 1;
						
						if (GameBoard[row-1][col-1] != 9)
							GameBoard[row-1][col-1] += 1;
						
						if (GameBoard[row-1][col+1] != 9)
							GameBoard[row-1][col+1] += 1;
						
						if (GameBoard[row+1][col-1] != 9)
							GameBoard[row+1][col-1] += 1;
						
						if (GameBoard[row+1][col+1] != 9)
							GameBoard[row+1][col+1] += 1;
					}
				}
			}
		}
	}
	
	private void AddPlayer() {
		GameBoard[xSize/2][ySize/2] = -1;
	}
	
	private void AddManBearPig() {
        /* Generate random coordinates for placing Manbearpig. Each time you play the game
         * Manbearpig will be in a different place, to add a challenging aspect to the game.
         */
		Random generator = new Random();
		int row = generator.nextInt()%xSize;
		int col = generator.nextInt()%ySize;
		if (row < 0) row *= -1;
		if (col < 0) col *= -1;
		GameBoard[row][col] = 13;
	}
	
	public void RevealEmptySpaces (int row, int col) {
        /* Using breadth-first search, reveal all empty spaces (indicated in the grid by
         * 0's) that have not already been revealed. Do not reveal any numbered spaces
         * or tar pits.
         */
		Queue <Point> queue = new LinkedList<Point>();
		Point p = new Point(row,col);
		queue.add(p);
		while (!queue.isEmpty()) {
			Point q = queue.poll();
			Visited[q.row][q.col]= true;
            //Check neighboring points and put them in the queue.
			if ((q.row >0 && q.row < xSize-1) && q.col>0 && q.col<ySize-1) {
				if (Visited[q.row-1][q.col-1] != true) {
					if (GameBoard[q.row-1][q.col-1] == 0) {
						//upper left
						Point r = new Point(q.row-1, q.col-1);
						queue.add(r);
					}
				}
				if (Visited[q.row-1][q.col]!= true) {
					if (GameBoard[q.row-1][q.col] == 0) {
						//upp
						Point s = new Point(q.row-1,q.col);
						queue.add(s);
					}
				}
				if (Visited[q.row][q.col-1] != true) {
					if (GameBoard[q.row][q.col-1] == 0) {
						//left
						Point t = new Point(q.row,q.col-1);
						queue.add(t);
					}
				}
				if (Visited[q.row+1][q.col]!= true) {
					if (GameBoard[q.row+1][q.col] == 0) {
						//lower
						Point a = new Point(q.row+1, q.col);
						queue.add(a);
					}
				}
				if (Visited[q.row][q.col+1] != true) {
					if (GameBoard[q.row][q.col+1] == 0) {
						//right
						Point b = new Point(q.row, q.col+1);
						queue.add(b);

					}
				}
				if (Visited[q.row+1][q.col+1] != true) {
					if (GameBoard[q.row+1][q.col+1] == 0) {
						//lower right
						Point c = new Point(q.row+1,q.col+1);
						queue.add(c);
					}
				}
				if (Visited[q.row-1][q.col+1] != true) {
					if (GameBoard[q.row-1][q.col+1] == 0) {
						Point d = new Point(q.row-1, q.col+1);
						queue.add(d);
					}
				}
			}
			checkVisited(row,col);
		}
	}
	
	private void checkVisited(int row, int col) {
        /* Reveal any places around your character that are not tar pits or Manbearpig */
		this.Visited[row][col] = true;
		if (row < xSize-1 && row > 0 && col < ySize-1 && col > 0) {
			if (GameBoard[row-1][col] != 9)
				this.Visited[row-1][col] = true;
			
			if (GameBoard[row+1][col] != 9)
				this.Visited[row+1][col] = true;
			
			if (GameBoard[row][col-1] != 9)
				this.Visited[row][col-1] = true;
			
			if (GameBoard[row][col+1] != 9)
				this.Visited[row][col+1] = true;
			
			if (GameBoard[row-1][col-1] != 9)
				this.Visited[row-1][col-1] = true;
			
			if (GameBoard[row-1][col+1] != 9)
				this.Visited[row-1][col+1] = true;
			
			if (GameBoard[row+1][col-1] != 9)
				this.Visited[row+1][col-1] = true;
			
			if (GameBoard[row+1][col+1] != 9)
				this.Visited[row+1][col+1] = true;
		}
	}
}