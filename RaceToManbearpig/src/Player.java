public class Player {

	private int playerOldX, playerOldY, playerNewX, playerNewY, numMoves;

	Player(int x, int y, int gridSize) {
		playerOldX = x/2;
		playerOldY = y/2;

		playerNewX = playerOldX;
		playerNewY = playerOldY;

		numMoves = gridSize/2;
	}

	public int getNumMoves() { return numMoves; }
	public int getPlayerNewX() { return playerNewX; }
	public int getPlayerNewY() { return playerNewY; }

	public void removeOneMove() { numMoves--; }

	public void moveUp() {
		playerOldX = playerNewX;
		playerNewX--;
	}

	public void moveLeft() {
		playerOldY = playerNewY;
		playerNewY--;
	}

	public void moveDown() {
		playerOldX = playerNewX;
		playerNewX++;
	}

	public void moveRight() {
		playerOldY = playerNewY;
		playerNewY++;
	}
}