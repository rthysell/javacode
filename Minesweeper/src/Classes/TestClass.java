package Classes;

import Interfaces.IBoard;
import Interfaces.IDisplay;
import Interfaces.IGame;

public class TestClass {

	public static void main(String[] args) 
	{
		IBoard b = new Board( 10, 10 );
		b.InitializeBoard();
		b.PopulateWithMines( 10 );
		b.AddNumbers();
		
		IDisplay d = new Display();
		
		IGame g = new Game( d, b );
		
	
	}

}
