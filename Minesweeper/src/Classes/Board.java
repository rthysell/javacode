package Classes;

import Interfaces.IBoard;
import java.util.*;

public class Board implements IBoard 
{
	private int[][] mBoard;
	private boolean[][] mVisited;
	private int mRows, mCols;
	private static final int MINE = 13;
	private static final int EMPTY = 0;
	
	public Board( int numRows, int numCols )
	{
		mRows = numRows;
		mCols = numCols;
		
		mBoard = new int[ mRows ][ mCols ];
		mVisited = new boolean[ mRows ][ mCols ];
	}
	
	@Override
	public void InitializeBoard()
	{
		for( int r = 0; r < mRows; r++ )
		{
			for( int c = 0; c < mCols; c++ )
			{
				mBoard[ r ][ c ] = EMPTY;
				mVisited[ r ][ c ] = false;
			}
		}
	}
	
	@Override
	public void PopulateWithMines( int numMines )
	{
		Random rand = new Random();
		int r, c;
		for( int i = 0; i < numMines; i++ )
		{
			r = rand.nextInt( mRows - 1 );
			c = rand.nextInt( mCols - 1 );
			mBoard[ r ][ c ] = MINE;
		}
	}
	
	@Override
	public void AddNumbers()
	{
		for( int r = 0; r < mRows; r++ )
		{
			for( int c = 0; c < mCols; c++ )
			{
				for( int i = -1; i < 2; i++ )
				{			
					for( int j = -1; j < 2; j++ )
					{
						if( ( r + i > -1 && r + i < mRows - 1) && 
								( c + j > -1 && c + j < mCols - 1 ) && 
								mBoard[ r + i ][ c + j ] == MINE && mBoard[ r ][ c ] != MINE )
						{
							mBoard[ r ][ c ] += 1;
						}
					}
				}
			}
		}
	}
	
	@Override
	public void PrintBoard()
	{
		for( int r = 0; r < mRows; r++ )
		{
			for( int c = 0; c < mCols; c++ )
			{
				System.out.print( "|\t" + mBoard[ r ][ c ] + "\t");
			}
			System.out.println( "|" );
		}
	}
	
	@Override
	public int[][] GetBoard()
	{
		return mBoard;
	}
	
	@Override
	public int GetNumRows()
	{
		return mRows;
	}
	
	@Override
	public int GetNumCols()
	{
		return mCols;
	}
	
	public boolean IsExposed( int r, int c )
	{
		if( mVisited[ r ][ c ] )
		{
			return true;
		}
		return false;
	}
	
	public void ExposeSpace( int r, int c )
	{
		mVisited[ r ][ c ] = true;
	}
	
	@Override
	public boolean IsMine( int r, int c )
	{
		if( mBoard[ r ][ c ] == MINE )
		{
			return true;
		}
		return false;
	}
	
	@Override
	public boolean IsNumber( int r, int c )
	{
		if( mBoard[ r ][ c ] > 0 && mBoard[ r ][ c ] != MINE )
		{
			return true;
		}
		return false;
	}
	
	@Override
	public boolean IsEmpty( int r, int c )
	{
		if( mBoard[ r ][ c ] == EMPTY )
		{
			return true;
		}
		return false;
	}
	
	public boolean[][] GetVisited()
	{
		return mVisited;
	}
}
