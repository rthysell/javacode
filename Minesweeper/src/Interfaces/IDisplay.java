package Interfaces;

public interface IDisplay 
{
	void PrintInitialBoard( int rows, int cols );
	
	void RevealExposedSpaces( IBoard b );
	
	void RevealMines( IBoard b );
	
	void UpdateBoard( IBoard b );
}
