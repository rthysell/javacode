
public class Card 
{
	private int cardNum, originalNum;
	private String cardSuit;
	
	// Write a class invariant that ensures num is within the limits
	public Card( int num )
	{
		originalNum = num;
		cardNum = num % 13;
	}
	
	public String getCardSuit()
	{
		if( originalNum >= 0 && originalNum <= 12 )
		{
			return "Hearts";
		}
		else if( originalNum > 12 && originalNum <= 25 )
		{
			return "Diamonds";
		}
		else if( originalNum > 25 && originalNum <= 38 )
		{
			return "Clubs";
		}
		else
		{
			return "Spades";
		}
	}
	
	public String getFaceValue()
	{
		switch( cardNum )
		{
			case 0:
				return "Two";
			case 1:
				return "Three";
			case 2:
				return "Four";
			case 3:
				return "Five";
			case 4:
				return "Six";
			case 5:
				return "Seven";
			case 6:
				return "Eight";
			case 7:
				return "Nine";
			case 8:
				return "Ten";
			case 9:
				return "Jack";
			case 10:
				return "Queen";
			case 11:
				return "King";
			case 12:
				return "Ace";
			default:
				return "Invalid selection";
		}
	}
	
	public String toString()
	{
		return getFaceValue() + " of " + getCardSuit();
	}
	
	public static int compare( int cardOne, int cardTwo )
	{
		if( cardOne == cardTwo )
		{
			return 0;
		}
		if( cardOne < cardTwo )
		{
			return -1;
		}
		if( cardOne > cardTwo )
		{
			return 1;
		}
		return -2;
	}
}
