import java.util.Random;
import java.util.Scanner;

public class Game {

	public static void main(String[] args) 
	{
		Scanner s = new Scanner( System.in );
		Random r = new Random();
		int cardOne, cardTwo;
		
		System.out.print( "Enter a 'D' to deal or an 'X' to exit: " );
		String choice = s.nextLine();
		
		while( choice.equals( "D" ) )
		{
			choice = "";
			cardOne = r.nextInt( 51 );
			cardTwo = r.nextInt( 51 );
			
			System.out.println( "Card one is " + cardOne + ", card two is " + cardTwo );
			
			while( cardOne == cardTwo )
			{
				cardOne = r.nextInt( 51 );
				cardTwo = r.nextInt( 51 );
			}
			
			Card playerOne = new Card( cardOne );
			Card playerTwo = new Card( cardTwo );
			
			System.out.println( "Player One: " + playerOne.toString() );
			System.out.println( "Player Two: " + playerTwo.toString() );
			
			int result = playerOne.compare( cardOne, cardTwo);
			
			switch( result )
			{
				case -1:
					System.out.println( "Player Two wins!" );
					break;
				case 0:
					System.out.println( "Tie" );
					break;
				case 1:
					System.out.println( "Player One wins!" );
					break;
			}
			
			System.out.print( "To play again, enter 'D', otherwise enter 'X' to quit." );
			choice = s.next();
		}
	}

}
